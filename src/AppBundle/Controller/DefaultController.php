<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Form\UserType;
use Symfony\Component\HttpFoundation\Request;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * * param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request ) 
    {
        $advertisements = $this->getDoctrine()
        ->getRepository('AppBundle:Advertisement')
        ->findBy(
            array(),
            array('id' => 'DESC')
            );
        return $this->render('AppBundle:Main:main.html.twig', array(
            'Advertisements' => $advertisements,
        ));
}

/**
 * @Route("/list", name="list")
 * * param Request $request
 * @return \Symfony\Component\HttpFoundation\Response
 */
public function listAction(Request $request)
{
    $userName = $this->get('security.token_storage')->getToken()->getUser()->getUsername();
    $advertisements = $this->getDoctrine()
    ->getRepository('AppBundle:Advertisement')
    ->findByName($userName);
    
    return $this->render('AppBundle:List:list.html.twig', array(
        'Advertisements' => $advertisements,
    ));
}

}
