<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Advertisement;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Form\AdvertisementType;
use AppBundle\Form\UserType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;

class AdvertisementController extends Controller
{
    /**
     * @Route("/newAdvertisement")
     * param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function advertisementAction(Request $request )
    {
        $em = $this->getDoctrine()->getManager();
        $advertisement = new Advertisement();
        $form = $this->createForm(AdvertisementType::class, $advertisement);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid() ){
            $userName = $this->get('security.token_storage')->getToken()->getUser()->getUsername();
            $advertisement->setName($userName);
            $em->persist($advertisement);
            $em->flush();
            return $this->redirectToRoute('homepage');
        }
        return $this->render('AppBundle:Advertisement:advertisement.html.twig', array(
            'form' => $form->createView(),
        ));
    }
    
}
