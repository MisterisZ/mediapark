<?php
namespace AppBundle\Form;

use AppBundle\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\DependencyInjection\Compiler\RepeatedPass;

class UserType extends AbstractType{
    
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder
        ->add('username',TextType::class,[
            'attr'=>[
                'class'=>'form-control',
                'maxlength'=>255
            ]
        ])
        ->add('email',EmailType::class,[
            'attr'=>[
                'class'=>'form-control',
                'maxlength'=>255
            ]
        ])
        ->add('password', RepeatedType::class, array(
            'type' => PasswordType::class,
            'invalid_message' => 'The password fields must match.',
            'first_options'  => array(
                'label' => 'Password',
                'attr' => array('class' => 'form-control')
            ),
            'second_options'  => array(
                'label' => 'Password',
                'attr' => array('class' => 'form-control')
            ),
            'attr' => array('class' => 'form-control')
        ))
        ; 
    }

    
}
?>