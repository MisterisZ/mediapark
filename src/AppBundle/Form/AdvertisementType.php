<?php
namespace AppBundle\Form;

use AppBundle\Entity\Advertisement;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\DependencyInjection\Compiler\RepeatedPass;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class AdvertisementType extends AbstractType{
    
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder
        ->add('title',TextType::class,[
            'attr'=>[
                'class'=>'form-control',
                'required' => true,
                'maxlength'=>255
            ]
        ])
        ->add('description',TextareaType::class,[
            'attr'=>[
                'class'=>'form-control',
                'style'=> 'resize: vertical;',
                 'required' => true
            ]
        ]);
    }
    
    
}
?>